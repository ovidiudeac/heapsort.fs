using System;
using System.Linq;
using BenchmarkDotNet.Attributes;

public class SortBenchmarks
{
    private static readonly Random random = new Random();
    private int[] _input;

//    [Params(1000000, 2000000, 4000000)]
//	[Params(10000000)]
	[Params(10000)]
    public int ArraySize { get; set; } = 0;

    [Setup]
    public void Setup()
    {
        _input = new int[ArraySize];
        for (var i = 0; i < ArraySize; ++i)
        {
            _input[i] = random.Next();
        }

    }
    [Benchmark(Baseline = true)]
    public object ArraySortInPlace()
    {
        return Heapsort.HeapModule.sort(_input).Count();
    }

    [Benchmark]
    public object ToMinHeap()
    {
        return Heapsort.HeapModule.toMinHeap(_input); // call the code you want to benchmark here
    }

    [Benchmark]
    public object HeapSort1percent()
    {
        var toTake = ArraySize / 100 * 1;
        return Heapsort.HeapModule.sort(_input).Take(toTake).Count();
    }

    [Benchmark]
    public object HeapSort10percent()
    {
        var toTake = ArraySize / 100 * 10;
        return Heapsort.HeapModule.sort(_input).Take(toTake).Count();
    }

    //[Benchmark]
    //public object HeapSort20percent()
    //{
    //    var toTake = ArraySize / 100 * 20;
    //    return Heapsort.HeapModule.sort(_input).Take(toTake).Count();
    //}

    //[Benchmark]
    //public object HeapSort30percent()
    //{
    //    var toTake = ArraySize / 100 * 30;
    //    return Heapsort.HeapModule.sort(_input).Take(toTake).Count();
    //}

    //[Benchmark]
    //public object HeapSort40percent()
    //{
    //    var toTake = ArraySize / 100 * 40;
    //    return Heapsort.HeapModule.sort(_input).Take(toTake).Count();
    //}


    //[Benchmark]
    //public object HeapSort50percent()
    //{
    //    var toTake = ArraySize / 100 * 50;
    //    return Heapsort.HeapModule.sort(_input).Take(toTake).Count();
    //}

    [Benchmark]
    public object HeapSortAll()
    {
        return Heapsort.HeapModule.sort(_input).Count();
    }
}