namespace Heapsort

type Heap = 
    {
        array : int []
        mutable rootIndex : int
        mutable maxIndex : int
    }

    member inline this.length = this.maxIndex - this.rootIndex

    member inline private this.nodeWithRoot index = 
        if index > this.maxIndex
        then None 
        else Some {this with rootIndex = index}
        
    member inline this.value = this.array.[this.rootIndex]

    member inline this.left = this.nodeWithRoot (this.rootIndex * 2 + 1)
    member inline this.right = this.nodeWithRoot (this.rootIndex * 2 + 2)

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Heap =
    let chunkSize = 1024 * 200 / sizeof<int>

    module private Array =
        let inline swap (a : 't []) i j =
            let v = a.[i]
            a.[i] <- a.[j]
            a.[j] <- v

    module private Option =
        let inline getOrElse elseValue =
            function
            | Some v -> v
            | None -> elseValue

    let rec private shiftDown (input : Heap): unit =
        let right = input.right |> Option.getOrElse input
        let left = input.left |> Option.getOrElse input

        let minChild = if right.value < left.value then right else left

        if minChild.value < input.value
        then
            do Array.swap input.array input.rootIndex minChild.rootIndex
            do shiftDown minChild

    let minHeapify (h : Heap) =
        for index = h.length / 2 downto 0 do
            h.rootIndex <- index
            shiftDown h

    
    let toMinHeap (a : int[]) =
        let h = {array = a; rootIndex = 0; maxIndex = a.Length - 1}
        do minHeapify h
        h

    let sort (a : int[]) = 
        let rec toSortedSeq (h : Heap) = 
            seq {
                yield h.value
                if h.maxIndex > 0 then
                    do Array.swap h.array 0 h.maxIndex
                    h.maxIndex <- h.maxIndex - 1
                    do shiftDown h
                    yield! toSortedSeq h
            }

        a
        |> toMinHeap
        |> toSortedSeq